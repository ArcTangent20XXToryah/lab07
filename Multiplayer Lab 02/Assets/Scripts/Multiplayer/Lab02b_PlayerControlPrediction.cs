﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class Lab02b_PlayerControlPrediction : NetworkBehaviour
{
    public float walkSpeed;
    public float runSpeed;
    public float rotationSpeed;
    public float jumpHeight;
    public float jumpSpeed;
    public float fallSpeed;
    public enum CharacterState
    {
        Idle = 0,
        WalkingForward = 1,
        WalkingBackwards = 2,
        Jumping = 4,
        RunningForward = 5,
        RunningBackwards = 6
    }

    struct PlayerState
    {
        public int movementNumber;
        public float posX, posY, posZ;
        public float rotX, rotY, rotZ;
        public CharacterState animationState;
    }

    [SyncVar(hook = "OnServerStateChanged")]
    PlayerState serverState;

    PlayerState predictedState;

    Queue<KeyCode> pendingMoves;

    bool falling = false;

    CharacterState characterAnimationState;
    public Animator controller;

    void Start()
    {
        InitState();
        predictedState = serverState;
        if(isLocalPlayer)
        {
            pendingMoves = new Queue<KeyCode>();
            UpdatePredictedState();
        }
        SyncState();
    }
    void Update()
    {
        if (isLocalPlayer)
        {
            Debug.Log("Pending moves: " + pendingMoves.Count);
            KeyCode[] possibleKeys = { KeyCode.A, KeyCode.S, KeyCode.D, KeyCode.W, KeyCode.Q, KeyCode.E, KeyCode.Space, KeyCode.J, KeyCode.K };
            bool somethingPressed = false;
            foreach (KeyCode possibleKey in possibleKeys)
            {
                if (!Input.GetKey(possibleKey))
                    continue;
                somethingPressed = true;
                pendingMoves.Enqueue(possibleKey);
                UpdatePredictedState();
                CmdMoveOnServer(possibleKey);
            }
            if(!somethingPressed)
            {
                pendingMoves.Enqueue(KeyCode.Alpha0);
                UpdatePredictedState();
                CmdMoveOnServer(KeyCode.Alpha0);
            }
        }
        SyncState();
    }

    [Command]
    void CmdMoveOnServer(KeyCode pressedKey)
    {
        serverState = new PlayerState
        {
            movementNumber = serverState.movementNumber,
            posX = transform.localPosition.x,
            posY = serverState.posY,
            posZ = transform.localPosition.z,
            rotX = transform.localRotation.x,
            rotY = serverState.rotY,
            rotZ = transform.localRotation.z,
            animationState = serverState.animationState
        };
        serverState = Move(serverState, pressedKey);
    }

    void InitState()
    {
        serverState = new PlayerState
        {
            movementNumber = 0,
            posX = -119f,
            posY = 165.08f,
            posZ = -924f,
            rotX = 0f,
            rotY = 0f,
            rotZ = 0f
        };
    }

    void SyncState()
    {
        PlayerState stateToRender = isLocalPlayer ? predictedState : serverState;
        transform.position = new Vector3(stateToRender.posX, stateToRender.posY, stateToRender.posZ);
        transform.rotation = Quaternion.Euler(stateToRender.rotX, stateToRender.rotY, stateToRender.rotZ);
        controller.SetInteger("CharacterState", (int)stateToRender.animationState);
    }


    PlayerState Move(PlayerState previous, KeyCode newKey)
    {
        float deltaX = 0, deltaY = 0, deltaZ = 0;
        float deltaRotationY = 0;
        bool jumping = false;
        bool doingNothing = false;
        switch (newKey)
        {
            case KeyCode.Q:
                deltaX = - walkSpeed;
                jumping = false;
                break;
            case KeyCode.S:
                deltaZ = - walkSpeed;
                jumping = false;
                break;
            case KeyCode.E:
                deltaX = walkSpeed;
                jumping = false;
                break;
            case KeyCode.W:
                deltaZ = walkSpeed;
                jumping = false;
                break;
            case KeyCode.A:
                deltaRotationY = -rotationSpeed;
                jumping = false;
                break;
            case KeyCode.D:
                deltaRotationY = rotationSpeed;
                jumping = false;
                break;
            case KeyCode.Space:
                deltaY = jumpSpeed;
                jumping = true;
                break;
            case KeyCode.K:
                deltaZ = -runSpeed;
                jumping = false;
                break;
            case KeyCode.J:
                deltaZ = runSpeed;
                jumping = false;
                break;
            default:
                doingNothing = true;
                jumping = true;
                break;
        }
        if(previous.posY >= 165.08f + jumpHeight || deltaY == 0 && previous.posY > 165.08f && jumping == true && doingNothing == true)
        {
            falling = true;
        }
        if (previous.posY > 165.08f && falling == true)
        {
            deltaY = -fallSpeed;
        }
        if (previous.posY <= 165.08f && falling == true)
        {
            deltaY = 165.08f - previous.posY;
            falling = false;
        }

        //get b over the sin of B
        //if(deltaRotationY != previous.rotY)
        //{
        //    return new PlayerState
        //    {
        //        movementNumber = 1 + previous.movementNumber,
        //        posX = deltaX + previous.posX,
        //        posY = deltaY + previous.posY,
        //        posZ = deltaZ + previous.posZ,
        //        rotX = previous.rotX,
        //        rotY = deltaRotationY + previous.rotY,
        //        rotZ = previous.rotZ,
        //        animationState = CalcAnimation(deltaX, deltaY, deltaZ, deltaRotationY)
        //    };
        //}
        //else
        //{
            return new PlayerState
            {
                movementNumber = 1 + previous.movementNumber,
                posX = deltaX * Mathf.Cos(deltaRotationY)+ previous.posX,
                posY = deltaY + previous.posY,
                posZ = deltaZ  + previous.posX,
                rotX = previous.rotX,
                rotY = deltaRotationY + previous.rotY,
                rotZ = previous.rotZ,
                animationState = CalcAnimation(deltaX, deltaY, deltaZ, deltaRotationY)
            };
        //}
    }

    void OnServerStateChanged(PlayerState newState)
    {
        serverState = newState;
        if (pendingMoves != null)
        {
            while (pendingMoves.Count > (predictedState.movementNumber - serverState.movementNumber))
            {
                pendingMoves.Dequeue();
            }
            UpdatePredictedState();
        }
    }

    void UpdatePredictedState()
    {
        predictedState = serverState;
        foreach(KeyCode moveKey in pendingMoves)
        {
            predictedState = Move(predictedState, moveKey);
        }
    }

    CharacterState CalcAnimation(float dx, float dy, float dz, float dRY)
    {
        if (dx == 0 && dy == 0 && dz == 0)
            return CharacterState.Idle;
        if(dx != 0 || dz != 0)
        {
            if (dx == walkSpeed || dz == walkSpeed)
                return CharacterState.WalkingForward;
            if (dx == -walkSpeed || dz == -walkSpeed)
                return CharacterState.WalkingBackwards;
            if (dx == runSpeed || dz == runSpeed)
                return CharacterState.RunningForward;
            if (dx == -runSpeed || dz == -runSpeed)
                return CharacterState.RunningBackwards;
        }

        return CharacterState.Idle;
    }
}
